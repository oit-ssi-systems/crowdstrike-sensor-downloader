#!/usr/bin/env python3
import sys
import argparse
import hvac
import requests
import logging
import os
import yaml
import subprocess
import smtplib
from jinja2 import Template
from email.message import EmailMessage


def parse_args():
    """Parse the arguments."""
    parser = argparse.ArgumentParser(description="Download Crowdstrike sensor packages")
    parser.add_argument(
        "-v", "--verbose", help="Be verbose", action="store_true", dest="verbose"
    )
    parser.add_argument(
        "-u",
        "--url",
        help="Base URL for Crowdstrike api",
        default="https://api.crowdstrike.com",
    )
    parser.add_argument(
        "-p",
        "--platforms-yaml",
        help="YAML file containing platform info",
        default="./platforms.yaml",
    )
    parser.add_argument(
        "-b",
        "--base-url",
        help="Base URL for downloading",
        default="https://falcon-dl.oit.duke.edu",
    )

    parser.add_argument(
        "-c",
        "--check",
        action="store_true",
        help="Don't actually do anything, just print what you would do",
    )

    parser.add_argument(
        "-d",
        "--destination",
        default="./pkgs",
        help="Destination to download packages to",
    )

    return parser.parse_args()


def main():
    args = parse_args()
    logging.basicConfig(level=logging.INFO)
    crowdstrike_url = args.url
    mail_server = smtplib.SMTP("smtp.duke.edu", 25)

    # Get template for MR ready
    t = open(
        os.path.join(
            os.path.dirname(os.path.abspath(__file__)), "../templates/email.j2"
        ),
        "r",
    )
    mail_template = Template(t.read())
    t.close()

    if args.check:
        logging.info("Running in check mode, no actual changes made")

    with open(args.platforms_yaml, "r") as p_yaml:
        platform_yaml = yaml.load(p_yaml.read())

    vault = hvac.Client()
    if "VAULT_TOKEN" in os.environ:
        logging.info("Using VAULT_TOKEN")
        vault.token = os.environ["VAULT_TOKEN"]
    elif "VAULT_OKD_ROLE" in os.environ:
        logging.debug("Using K8s auth")
        f = open("/var/run/secrets/kubernetes.io/serviceaccount/token")
        jwt = f.read()
        vault.auth_kubernetes(
            os.environ["VAULT_OKD_ROLE"], jwt, mount_point="global/okd3-fitz"
        )
    else:
        logging.info("Using VAULT_ROLE_ID and VAULT_SECRET_ID")
        vault.auth_approle(os.environ["VAULT_ROLE_ID"], os.environ["VAULT_SECRET_ID"])

    creds = vault.read("ssi-systems/kv/data/shared_service_accounts/crowdstrike-sensor-downloader")[
        "data"
    ]['data']

    # Import gpg keys
    subprocess.check_call("echo '%s' | gpg --import" % creds["gpg_public"], shell=True)
    subprocess.check_call("echo '%s' | gpg --import" % creds["gpg_private"], shell=True)

    oauth_headers = {
        "accept": "application/json",
        "Content-Type": "application/x-www-form-urlencoded",
    }
    auth = requests.post(
        f"{crowdstrike_url}/oauth2/token",
        data={"client_id": creds["client_id"], "client_secret": creds["client_secret"]},
        headers=oauth_headers,
    ).json()
    token = auth["access_token"]

    # Find all the installers

    # Set headers for queries
    headers = {"accept": "application/json", "authorization": f"bearer {token}"}
    for platform, platform_items in platform_yaml.items():
        blocklist = platform_items.get("blocklist", [])
        new_packages = []
        for label, data in platform_items["flavors"].items():

            filters = data.get("filters", {})

            logging.info(f"Querying {label} with {filters}")
            pkg_dir = f"{args.destination}/{label}"
            if not os.path.exists(pkg_dir):
                logging.info(f"Creating {pkg_dir}")
                if not args.check:
                    os.mkdir(pkg_dir)
            q = requests.get(
                f"{crowdstrike_url}/sensors/combined/installers/v1",
                headers=headers,
                params={
                    "filter": 'platform:"%s"' % platform,
                    "sort": "release_date|desc",
                    "limit": 500,
                },
            )
            for resource in q.json()["resources"]:
                is_valid = True
                for filter_k, filter_v in filters.items():
                    if resource[filter_k] != filter_v:
                        is_valid = False

                if not is_valid:
                    continue

                # Note, all windows installers have the same file name, so let's
                # put them in a versioned directory

                if platform in ["windows", "mac"]:
                    download_pkg_dir = os.path.join(pkg_dir, resource["version"])
                    if not os.path.exists(download_pkg_dir):
                        logging.info(f"Creating {download_pkg_dir}")
                        if not args.check:
                            os.mkdir(download_pkg_dir)
                    pkg_path = os.path.join(download_pkg_dir, resource["name"])
                else:
                    pkg_path = os.path.join(pkg_dir, resource["name"])

                if os.path.exists(pkg_path) and (resource["version"] in blocklist):
                    logging.info(
                        f"{pkg_path} has already been downloaded, but it is blocklisted, removing"
                    )
                    if not args.check:
                        os.remove(pkg_path)
                elif os.path.exists(pkg_path) and (
                    resource["version"] not in blocklist
                ):
                    logging.debug(f"{pkg_path} has already been downloaded")
                elif not os.path.exists(pkg_path) and (
                    resource["version"] not in blocklist
                ):

                    logging.info(f"Downloading {resource['name']}")

                    new_packages.append(
                        "%s%s" % (args.base_url, pkg_path.replace(args.destination, ""))
                    )
                    if not args.check:
                        installer = requests.get(
                            f"{crowdstrike_url}/sensors/entities/download-installer/v1",
                            headers=headers,
                            params={"id": resource["sha256"]},
                        )
                        pkg_path_sha = "%s.sha256" % pkg_path
                        with open(pkg_path, "wb") as pkg_f:
                            pkg_f.write(installer.content)
                        with open(pkg_path_sha, "w") as pkg_f_sha:
                            pkg_f_sha.write("%s\n" % resource["sha256"])

            indexer_cmd = data.get("indexer_command")
            if not args.check and indexer_cmd:
                logging.info("Creating packaging repos: %s" % pkg_dir)
                subprocess.check_call(
                    indexer_cmd.format(**{"pkg_dir": pkg_dir}), shell=True
                )

        # Send a notification sometimes
    #       if not args.check:
    #           if (len(new_packages) > 0) and (len(platform_items.get("notify", [])) > 0):
    #               logging.info("Sending a package notification out")
    #               msg = EmailMessage()
    #               from_addy = "endpoints-no-reply@duke.edu"
    #               to_addy = ",".join(platform_items.get("notify"))
    #               msg["From"] = from_addy
    #               msg["To"] = to_addy
    #               msg["Subject"] = f"New installers for Crowdstrike {platform}"
    #               body = mail_template.render(
    #                   new_packages=new_packages, platform=platform, base_url=args.base_url
    #               )
    #               msg.set_content(body)
    #               mail_server.send_message(msg)

    if not args.check:
        logging.info("Creating documentation html")
        subprocess.check_call(
            [
                "pandoc",
                "-s",
                "/code/README.md",
                "-o",
                "%s/README.html" % args.destination,
            ]
        )
        logging.info("Creating index.html from tree")
        index_info = subprocess.check_output(
            [
                "tree",
                "--noreport",
                "-T",
                "Duke Falcon Sensor Downloads",
                "-I",
                "conf|db|index.html",
                "-H",
                "",
                args.destination,
            ]
        )
        with open("%s/%s" % (args.destination, "index.html"), "w") as index_f:
            index_f.write(index_info.decode())
    return 0


if __name__ == "__main__":
    sys.exit(main())
