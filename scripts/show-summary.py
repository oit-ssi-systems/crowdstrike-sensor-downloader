#!/usr/bin/env python3
import sys
import argparse
import hvac
import requests
import logging
import os


def parse_args():
    """Parse the arguments."""
    parser = argparse.ArgumentParser(description="Download Crowdstrike sensor packages")
    parser.add_argument(
        "-v", "--verbose", help="Be verbose", action="store_true", dest="verbose"
    )
    parser.add_argument(
        "-u",
        "--url",
        help="Base URL for Crowdstrike api",
        default="https://api.crowdstrike.com",
    )

    return parser.parse_args()


def main():
    args = parse_args()
    logging.basicConfig(level=logging.INFO)
    crowdstrike_url = args.url

    vault = hvac.Client()
    if "VAULT_ROLE_ID" in os.environ:
        logging.info("Using VAULT_ROLE_ID and VAULT_SECRET_ID")
        vault.auth_approle(os.environ["VAULT_ROLE_ID"], os.environ["VAULT_SECRET_ID"])

    creds = vault.read("ssi-systems/kv/data/shared_service_accounts/crowdstrike-sensor-downloader")[
        "data"
    ]['data']

    oauth_headers = {
        "accept": "application/json",
        "Content-Type": "application/x-www-form-urlencoded",
    }
    auth = requests.post(
        f"{crowdstrike_url}/oauth2/token",
        data={"client_id": creds["client_id"], "client_secret": creds["client_secret"]},
        headers=oauth_headers,
    ).json()
    token = auth["access_token"]

    # Find all the installers

    # Set headers for queries
    headers = {"accept": "application/json", "authorization": f"bearer {token}"}
    flavors = {}
    for platform in ["linux", "windows", "mac"]:
        flavors[platform] = []
        q = requests.get(
            f"{crowdstrike_url}/sensors/combined/installers/v1",
            headers=headers,
            params={
                "filter": 'platform:"%s"' % platform,
                "sort": "release_date|desc",
                "limit": 500,
            },
        )
        for item in q.json()["resources"]:
            scrubbed_item = {
                "description": item["description"],
                "os": item["os"],
                "os_version": item["os_version"],
                "file_type": item["file_type"],
            }
            if scrubbed_item not in flavors[platform]:
                flavors[platform].append(scrubbed_item)

    for platform, flavor_info in flavors.items():
        print(platform)
        for f in flavor_info:
            print(" ", f)
        print()
    return 0


if __name__ == "__main__":
    sys.exit(main())
