#FROM image-mirror-prod-registry.cloud.duke.edu/library/ubuntu:hirsute-20210422
FROM image-mirror-prod-registry.cloud.duke.edu/library/ubuntu:22.04

RUN mkdir /code
COPY . /code
WORKDIR /code

ENV VAULT_ADDR="https://vault-systems.oit.duke.edu"
ENV DEBIAN_FRONTEND noninteractive

RUN apt-get -y update && \
    apt-get -y install --no-install-recommends \
        wget unzip curl reprepro ca-certificates openssh-client jq rpm \
        python3-pip git python3-yaml gnupg tree createrepo-c pandoc && \
    python3 -m pip install -r ./requirements.txt && \
    mkdir /.gnupg && \
    chgrp -R 0 /.gnupg && \
    chmod -R g+rwX,o-rwx /.gnupg && \
    rm -rf /var/lib/apt/lists/*
