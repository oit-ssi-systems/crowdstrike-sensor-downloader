# Crowdstrike Sensor Downloader

## Overview

This is a python script that will reach out to the Crowdstrike API, and
download packages based on a given platform, and additional query parameters

## Connecting Clients

### RHEL/CentOS/etc

```
[falcon-sensor-el7]
name=Falcon Sensor for Crowdstrike
baseurl=https://falcon-dl.oit.duke.edu/rhel7/
gpgcheck=1
gpgkey=https://falcon-dl.oit.duke.edu/pubkeys/falcon-sensor.gpg
enabled=1
```

```
[falcon-sensor-el8]
name=Falcon Sensor for Crowdstrike
baseurl=https://falcon-dl.oit.duke.edu/rhel8/
gpgcheck=1
gpgkey=https://falcon-dl.oit.duke.edu/pubkeys/falcon-sensor.gpg
enabled=1
```

### Ubuntu

Add the public key to your system

```
curl https://falcon-dl.oit.duke.edu/pubkeys/falcon-dl.oit.duke.edu.gpg | apt-key add -
```

Add the following line to `/etc/apt/sources.list`:

For 18.04:
```
deb https://falcon-dl.oit.duke.edu/ubuntu bionic main
```

For 16.04:
```
deb https://falcon-dl.oit.duke.edu/ubuntu xenial main
```


## Configuration for this repository

Code for this repository is [here](https://gitlab.oit.duke.edu/oit-ssi-systems/crowdstrike-sensor-downloader)

Downloads are configured in `platforms.yaml`, it should look something like:

```yaml
---
linux:
  rhel7:
    os: "RHEL/CentOS/Oracle"
    os_version: "7"
  rhel8:
    os: "RHEL/CentOS/Oracle"
    os_version: "8"
  ubuntu:
    os: "Ubuntu"
    os_version: "14/16/18"
windows:
  windows: {}
```

The base keys are the 'platform' that is required in our filter (Example: 'linux')

The next key is a friendly name for the distro (Example: 'rhel7')

Following optional keys are filters

## Documentation

[API Overview](https://supportportal.crowdstrike.com/s/article/Release-Notes-Download-Sensor-Installers-Via-API)

[Example Requests](https://falcon.crowdstrike.com/support/documentation/109/sensor-download-apis)

[Swagger](https://assets.falcon.crowdstrike.com/support/api/swagger.html)

## Notes

Original CJ image: `image: docker-registry.default.svc:5000/crowdstrike-sensor-installers/crowdstrike-sensor-downloader:latest`
